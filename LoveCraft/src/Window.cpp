#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Window.h"
#include "Input.h"
#include <iostream>
//#include <string>

using std::cout;
using std::endl;



Window::Window()
{
    /*Always set initialization hints before glfwinit */
    glfwInitHint(GLFW_RESIZABLE, GLFW_TRUE);
    glfwInitHint(GLFW_VISIBLE, GLFW_TRUE);
    glfwInitHint(GLFW_FOCUSED, GLFW_TRUE);
    glfwInitHint(GLFW_AUTO_ICONIFY, GLFW_TRUE);
    glfwInitHint(GLFW_MAXIMIZED, GLFW_TRUE);
    glfwInitHint(GLFW_TRUE, GLFW_TRUE);
    glfwInitHint(GLFW_FOCUS_ON_SHOW, GLFW_TRUE);
    glfwInitHint(GLFW_SCALE_TO_MONITOR, GLFW_TRUE);

    if (!glfwInit()) {
        cout << "Error initializing the GLFW library." << endl;
    }

    // Create a window and context
    // Check GLFW version
    //glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 6);
    //glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);


    // Create window on primary monitor
    GLFWmonitor* primaryMonitor =  glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(primaryMonitor);
    windowptr = glfwCreateWindow(windowWidth, windowHeight, windowTitle, NULL, NULL);
    //glfwSetWindowMonitor(windowptr, primaryMonitor, 0, 0, mode->width, mode->height, mode->refreshRate);

    /*int mon_cont;
    GLFWmonitor** monitors = glfwGetMonitors(&mon_cont);
    cout << "Monitors before: " << mon_cont;*/
    //windowptr = glfwCreateWindow(windowWidth, windowHeight, windowTitle, NULL, NULL);

    if (!windowptr)
    {
        cout << "Window or OpenGL context creation failed" << endl;
        //glfwTerminate();
        return;
    }

    // Create context
    glfwMakeContextCurrent(windowptr);

    // Initialize OpenGL bindings
    if (glewInit() != GLEW_OK) {
        cout << "ERROR" << endl;
    }


    cout << "GL Version: " << glGetString(GL_VERSION) << endl;

    ////Set callback for closing window
    glfwSetWindowCloseCallback(windowptr, Window::window_close_callback);

    input.initKeyBindings(windowptr);
}

Window::~Window()
{
    cout << "Window object destroyed" << endl;
    cout << "Ending GLFW" << endl;
    glfwDestroyWindow(windowptr);
    glfwTerminate();
}

void Window::updateRender() {

    //obv needs to be updated with actual info
    float positions[6] = { -0.5f, -0.5f, 0.0f, 0.5f, 0.5f, -0.5f };

    unsigned int buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), positions, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0);
    glEnableVertexAttribArray(0);

    /* Render here */
    glClear(GL_COLOR_BUFFER_BIT);


    glDrawArrays(GL_TRIANGLES, 0, 3);


    /* Swap front and back buffers */
    glfwSwapBuffers(windowptr);

    /* Poll for and process events */
    glfwPollEvents();

    input.handleKeys(windowptr);
}

bool Window::getglfwWindowShouldClose(){
    return glfwWindowShouldClose(windowptr);
}


void Window::window_close_callback(GLFWwindow *windowptr)
{
    std::cout << "Window closing recognized" << std::endl;

    //Window* w = static_cast<Window*>(glfwGetWindowUserPointer(windowptr));
    if (windowptr) {
        cout << "window pointer still valid. Closing window";
        cout << " but program still running" << endl;
        cout << endl;
        glfwSetWindowShouldClose(windowptr, GLFW_TRUE);
    }

}



/*void Window::monitor_switch_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}*/

#include <GLFW/glfw3.h>
#include "Input.h"

#include <iostream>
using std::cout;
using std::endl;

Input::Input(){
    //

}

Input::~Input(){
	//
}

void Input::initKeyBindings(GLFWwindow* windowptr) {

    // Minimal Key Bindings
    cout << "Minimal key bindings" << endl;

    cout << "Stating in window mode" << endl;


    // Set user pointer to this Input object
    glfwSetWindowUserPointer(windowptr, this);

    // Set key call back function
    glfwSetKeyCallback(windowptr, Input::key_callback);
}

void Input::setKeyStatus(int key, bool status) {
    keyPressed[key] = status;
}

void Input::updateKey(GLFWwindow* windowptr, int key, int scancode, int action, int mods){
    cout << "Key pressed: " << key << endl;
    if (key == GLFW_KEY_UNKNOWN){
        return;
    }
    if (action == GLFW_PRESS){
        setKeyStatus(key, true);
    }
    else if (action == GLFW_RELEASE){
        setKeyStatus(key, false);
    }
}


void Input::handleKeys(GLFWwindow* windowptr) {
    if (keyPressed[GLFW_KEY_ESCAPE] && keyPressed[GLFW_KEY_LEFT_CONTROL]) {
        glfwSetWindowShouldClose(windowptr, GLFW_TRUE);
    }

}




//////////////////////////////////////////////
//                                          //
//          ~~ Danger: Static Zone ~~        //
//                                          //
//  Enter at your own risk! This area is     //
//  for static function declarations only.  //
//  If you're not sure what you're doing,    //
//  turn back now or you may trigger         //
//  undefined behavior and segfaults.        //
//                                          //
//////////////////////////////////////////////

int Input::current_monitor = 0;
bool Input::fullScreen = false;

void Input::toggleFullScreen(GLFWwindow* windowptr) {
       
    if (fullScreen) {
        const int windowWidth = 640;
        const int windowHeight = 480;
        glfwSetWindowMonitor(windowptr, NULL, 0, 0, windowWidth, windowHeight, GLFW_DONT_CARE);
        fullScreen = false;
    }
    else {
        int monitor_count;
        GLFWmonitor** monitors = glfwGetMonitors(&monitor_count);
        //// Get the video mode of the monitor you want to switch to
        const GLFWvidmode* mode = glfwGetVideoMode(monitors[current_monitor]);


        if (monitors && monitor_count >= current_monitor) {
            glfwSetWindowMonitor(windowptr, monitors[current_monitor], 0, 0, mode->width, mode->height, mode->refreshRate);
            fullScreen = true;
        }   
    }

}

void Input::key_callback(GLFWwindow* windowptr, int key, int scancode, int action, int mods)
{
    Input* input = static_cast<Input*>(glfwGetWindowUserPointer(windowptr));
    input->updateKey(windowptr, key, scancode, action, mods);

    if (key == GLFW_KEY_KP_ADD && action == GLFW_PRESS)
        swapMonitorsRight(windowptr);
    if (key == GLFW_KEY_KP_SUBTRACT && action == GLFW_PRESS)
        swapMonitorsLeft(windowptr);
    if (key == GLFW_KEY_KP_ENTER && action == GLFW_PRESS)
        toggleFullScreen(windowptr);
}



void Input::swapMonitorsRight(GLFWwindow* windowptr) {
    int monitor_count;
    GLFWmonitor  **monitors = glfwGetMonitors(&monitor_count);
    current_monitor = (current_monitor + 1) % monitor_count;

    cout << "Moving content to monitor number " << current_monitor;
    cout << "Monitors available: " << monitor_count << endl;
    if (monitors == nullptr) { cout << "monitors don't exist"; return; }
    //cout << "All monitors: " << monitors << endl;
    cout << endl;
    //cout << monitors[current_monitor] << endl;

    //// Get the video mode of the monitor you want to switch to
    const GLFWvidmode* mode = glfwGetVideoMode(monitors[current_monitor]);
    // Switch to the monitor
    glfwSetWindowMonitor(windowptr, monitors[current_monitor], 0, 0, mode->width, mode->height, mode->refreshRate);

}

void Input::swapMonitorsLeft(GLFWwindow* windowptr) {
    int monitor_count;
    GLFWmonitor** monitors = glfwGetMonitors(&monitor_count);
    current_monitor = current_monitor - 1 ;

    if (current_monitor < 0) { 
        current_monitor = monitor_count - 1; }

    cout << "Moving content to monitor number " << current_monitor;
    cout << "Monitors available: " << monitor_count << endl;
    if (monitors == nullptr) { cout << "monitors don't exist"; return; }
    //cout << "All monitors: " << monitors << endl;
    cout << endl;
    //cout << monitors[current_monitor] << endl;

    //// Get the video mode of the monitor you want to switch to
    const GLFWvidmode* mode = glfwGetVideoMode(monitors[current_monitor]);
    // Switch to the monitor
    glfwSetWindowMonitor(windowptr, monitors[current_monitor], 0, 0, mode->width, mode->height, mode->refreshRate);

}
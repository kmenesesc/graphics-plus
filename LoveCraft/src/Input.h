#pragma once

#include <GLFW/glfw3.h>
#include<array>

using std::array;

constexpr int numberOfKeys = 349;


class Input
{
public:
	Input();
	~Input();
	void initKeyBindings(GLFWwindow* windowptr);
	void handleKeys(GLFWwindow* windowptr);
	void setKeyStatus(int key, bool status);

private:
	array<bool, numberOfKeys> keyPressed = {};
	void updateKey(GLFWwindow* windowptr, int key, int scancode, int action, int mods);

	static int current_monitor;
	static bool fullScreen;
	static void key_callback(GLFWwindow* windowptr, int key, int scancode, int action, int mods);
	static void swapMonitorsRight(GLFWwindow* windowptr);
	static void swapMonitorsLeft(GLFWwindow* windowptr);
	static void toggleFullScreen(GLFWwindow* windowptr);

};


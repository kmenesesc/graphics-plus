#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Input.h"



//void window_close_callback(GLFWwindow* windowptr);

class Window
{
public:
	Window();
	~Window();
	bool getglfwWindowShouldClose();
	void updateRender();
	//void createWindow();
private:
	GLFWwindow *windowptr;
	const int windowWidth = 640;
	const int windowHeight = 480;
	const char* const windowTitle = "LoveCraft";
	Input input;

	static void window_close_callback(GLFWwindow* windowptr); //needs to be static :(
	//static void monitor_switch_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
};
